#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char plaintext[PASS_LEN + 1];
    char hash[HASH_LEN];
};



// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    int lines = 10;
    
    FILE *dictFile = fopen(filename, "r");
    
    struct entry *entries = malloc(lines * sizeof(struct entry));
    char line[PASS_LEN + 1];
    int counter = 0;
    
    while(fgets(line, PASS_LEN + 1, dictFile))
    {
        if(lines == counter)
        {
            lines += 10;
            entries = realloc(entries, lines * sizeof(struct entry));
        }
        line[strlen(line)-1] = '\0';
        char *hashedLine = md5(line, strlen(line));
        strcpy(entries[counter].plaintext, line);
        strcpy(entries[counter].hash, hashedLine);
        counter ++;
        
        free(hashedLine);
    }
    
    fclose(dictFile);
    *size = counter;
    return entries;
}

int entryCompare(const void *a, const void *b)
{
    return strcmp((*(struct entry *)a).hash, (*(struct entry *)b).hash);
}



int bcomp(const void *target, const void *elem)
{
    return strcmp((char *)target, (*(struct entry *)elem).hash);
}



int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    FILE *dictFile = fopen(argv[2], "r");
    if(!dictFile)
    {
        printf("Unable to open %s for reading.", argv[2]);
        return 2;
    }
    fclose(dictFile);
    
    // TODO: Read the dictionary file into an array of entry structures
    int size;
    struct entry *dict = read_dictionary(argv[2], &size);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
     qsort(dict, size, sizeof(struct entry), entryCompare);

    // TODO
    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    if(!hashFile)
    {
        printf("Unable to open %s for reading.", argv[1]);
        return 2;
    }

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char line[PASS_LEN + 1];
    int counter = 0;
    
    while(fgets(line, PASS_LEN + 1, hashFile))
    {
        line[strlen(line)-1] = '\0';
        struct entry *found = bsearch(line, dict, size, sizeof(struct entry), bcomp);
        counter ++;
        
        if(found == NULL)
        {
            printf("%d %s --No Match Found--\n", counter, line);
        }
        
        else
        {
            printf("%d %s %s\n", counter, line, found->plaintext);
        }
    }
    
    fclose(hashFile);
    free(dict);
}
